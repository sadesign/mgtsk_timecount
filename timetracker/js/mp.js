$(document).ready(function () {
  var status = 'start';

  chrome.extension.sendRequest({key: 'key'}, function (response) {
    target = 'https://mgtsk.ru/acceptor/';
    mgtskKey = response.key;

    taskIdRaw = document.location.pathname;
    var myRe = /\/(task|project)\/(\d{4,7})\//;
    var taskId = myRe.exec(taskIdRaw);
    var taskIdN = taskId[2];

    var subjectType = taskId[1];
    var subjectId = taskId[2];

    $.ajax({
      url: target + 'info',
      type: 'POST',
      data: {key: mgtskKey},
      success: function (response) {
        if (response.info.adress == document.location.host) {
          console.log('Hi, I\'m MGTSK Time Count!');
          /*создаем кнопку*/
          $('#leftColumnMain .b-block:nth-child(2)').after('<div class="b-block tmc-wrapper" id="tmc-wrapper" />');
          $('#tmc-wrapper').append('<div class="g-inner"/>');
          $('#tmc-wrapper .g-inner').append('<button class="js-tmc_btn tmc_btn tmc_btn_default tmc-play"/>');
          $('#tmc-wrapper .tmc-play').append('<span class="tmc_btn-text"/>');
          $('#tmc-wrapper .tmc-play .tmc_btn-text').text('Считать время');
          $('#tmc-wrapper .tmc-play .tmc_btn-text').attr('data-text', 'Пауза');
        }
      },
      complete: function (xhr, status) {
        //console.log(xhr);
      }
    });
    /*проверяем состояние*/
    $.ajax({
      url: target + 'timeCount',
      type: 'POST',
      data: {
        key: mgtskKey, data: {
          SubjectType: subjectType,
//        SubjectId: subjectId,
          task: taskIdN,
          action: 'check'
        }
      },
      success: function (response) {
//      console.log(response)
        if (response.status.code == 'ok') {
          togglePausePlay();
          status = 'stop';
        }
      },
      error: function (response) {
        console.log(response)
      }
    });

    $(document).on('click', '.js-tmc_btn', function (e) {
      e.preventDefault();
      $(this).addClass('progress');
      if ($('.js-tmc_btn').hasClass('tmc-pause')) {
        status = 'stop';
      }
      $.ajax({
        url: target + 'timeCount',
        type: 'POST',
        data: {
          key: mgtskKey, data: {
            SubjectType: subjectType,
//                        SubjectId: subjectId,
            task: taskIdN,
            action: status,
          }
        },
        beforeSend: function () {
          $('.js-tmc_btn').prop('disabled', true);
        },
        success: function (response) {
          togglePausePlay();
          $('.js-tmc_btn').prop('disabled', false);
          if (response.status.code == "ok" && status == "stop") {
            location.reload();
          }
        },
        error: function (response) {
          console.log(response)
        },
        complete: function () {
          $('.js-tmc_btn').removeClass('progress');
        }
      });
    });

  });
});

function beforeSend() {
  $('.mgt').removeClass('mgt-drag-hidden');
}
function successSend() {
  $('.mgt').addClass('mgt-drag-hidden');
}
function togglePausePlay(el) {
  el = $('.tmc_btn');
  if ($(el).hasClass('tmc-pause')) {
    $(el).removeClass('tmc-pause');
    $(el).addClass('tmc-play');

    $(el).removeClass('tmc_btn_gray');
    $(el).addClass('tmc_btn_default');
  } else {
    $(el).removeClass('tmc-play');
    $(el).addClass('tmc-pause');

    $(el).removeClass('tmc_btn_default');
    $(el).addClass('tmc_btn_gray');
  }
  var oldText = $(el).find('.tmc_btn-text').text();
  var newText = $(el).find('.tmc_btn-text').attr('data-text');
  $(el).find('.tmc_btn-text').text(newText);
  $(el).find('.tmc_btn-text').attr('data-text', oldText);
}