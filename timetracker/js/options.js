$(document).ready(function () {
    function readProperty(property, defValue) {
        if (localStorage[property] == null) {
            return defValue;
        }

        return localStorage[property];
    }
    if (localStorage["mgtskKey"] && localStorage["mgtskKey"] !== '') {
        $('#localstorage-save').addClass('hidden');
        $('.js-haskey').removeClass('hidden');
    }
    $(".mgtskKey").each(function(i,el){
        el.value = readProperty("mgtskKey", '');
    })
           
    $('.js-haskey-change').on('click', function () {
        $('#localstorage-save').removeClass('hidden');
        $('.js-haskey').addClass('hidden');
    });
    $('form').submit(function () {
        var arr = $(this).serializeArray();
        arr.forEach(function (el, i) {
            localStorage[el.name] = el.value;
        });
//    return false;
    });
});